#include "FastLED.h"

// Configuration 
// wait time
int wait_time = 20000;

// wait time for each pixel 
int wait_time2 = 60;

// Led config 
#define NUM_LEDS 85
#define DATA_PIN 6

// vars
CRGB leds[NUM_LEDS];


// Pin layout for the sensors 
int mov_sensors[] = {2, 3}; // Pin of the movement sensors 

String mov_sensor_desc[] = {"oben", "unten"}; // Position of the sensors for the debuglog 
int kalibrierung_diode = 500;
//

// int daylight_sensor = 4;
bool active1 = false;
bool active3 = false;
int daylight_sensor = 0;

boolean active;
int mov_sensor_out[sizeof mov_sensors];

void setup() {
    Serial.begin(9600);
    
    FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS);

    pinMode(5, INPUT);
    for (int i = 0; i < (sizeof mov_sensors) / (sizeof mov_sensors[0]);i++)
    {

        pinMode(mov_sensors[i], INPUT);   
    }
    fill_solid( leds, NUM_LEDS, CRGB(0,0,0));
    FastLED.show();

    
}
void loop() {
    daylight_sensor = analogRead(A5);

    for (int y = 0; y < (sizeof mov_sensors) / (sizeof mov_sensors[0]); y++)
    {
        mov_sensor_out[y] = digitalRead(mov_sensors[y]);        
    }
    
    if (daylight_sensor >= kalibrierung_diode)
    {
        Serial.println("Dunkel warte auf Bewegunsmelder");

        for (int y = 0; y < (sizeof mov_sensors) / (sizeof mov_sensors[0]); y++)
        {   
            Serial.println(mov_sensor_out[y]);
            if(mov_sensor_out[y] == HIGH && !active)
            {
                active = true;
                Serial.print("Bewegung erkannt an dem Melder " );
                Serial.println(mov_sensor_desc[y]);
                
                if(mov_sensor_desc[y] == "oben")
                {
                    turn_leds_on(true, NUM_LEDS, leds);
                    delay(wait_time);
                    active = false;
                    turn_leds_off(true, NUM_LEDS, leds);   
                    
                }
                else if( mov_sensor_desc[y] == "unten" )
                {
                    turn_leds_on(false, NUM_LEDS, leds);
                    delay(wait_time);
                    active = false;
                    turn_leds_off(false, NUM_LEDS, leds);        
                };

        }
        

        };
    }else
    {
        Serial.println("Hell kein Betrieb");
        turn_leds_off(true,NUM_LEDS, leds);
    }      
    
}


void turn_leds_on(bool direction, int num_leds, CRGB leds[]){
    
    if(direction)
    {
        for(int dot=0 ; dot <= num_leds; dot++)
        {

            //FastLED.setBrightness(((100 / num_leds) * dot) * 2.5);

        
            leds[dot] = CRGB(100,40,0);
            FastLED.show();
            delay(wait_time2);
        }
    }
    else
    {
        for(int dot=num_leds ; dot >= 0; dot--)
        {

            //FastLED.setBrightness(((100 / num_leds) * (num_leds - dot)) * 2.5);

        
            leds[dot] = CRGB(100,40,0);
            FastLED.show();
            delay(wait_time2);
        }
    };
}

void turn_leds_off(bool direction, int num_leds, CRGB leds[]){
        if(direction)
    {
        for(int dot=0 ; dot <= num_leds; dot++)
        {

            //FastLED.setBrightness(255 / ((100 / num_leds) * dot));
        
            leds[dot] = CRGB(0,0,0);
            FastLED.show();
            delay(wait_time2);
        }
    }
    else
    {
        for(int dot=num_leds ; dot >= 0; dot--)
        {

             //FastLED.setBrightness(255 / ((100 / num_leds) * (num_leds - dot)));
        
            leds[dot] = CRGB(0,0,0);
            FastLED.show();
            delay(wait_time2);
        }
    };
}
